import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.DoubleBinaryOperator;
import java.util.function.Function;
import java.util.logging.*;
import java.util.logging.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;
import classfile.*;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.Constant.*;
import classfile.Pre;
import classfile.Rational;
import classfile.PNode;
import classfile.Polynomial;


import java.util.stream.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        test_addPoly();
        test1();
        test_multiply();
        test_evaluate();
        test_evaluate_poly();
        test_add();
        testfillzero();
        testnegate();
        test_degree();
        test_diff();
        test_poly_equals();
        test_normalList();
        test_Rational_equals();
        test_constructor_Rational();
        test_Rational_add();
        test_simplify();
        test_Rational_multi();
        test_negate();
        test_subtract();
        test_inverse();
        test_divide();
        test_compareTo();
        test_gt();
        test_lt();
        test_test();
        test_pow_int();
        test_pow_long();
        test_pow_Rational();

        test_99();
    }

    public static void test_test() {
        beg();
        {
        }
        end();
    }
    public static void test_lt() {
        beg();
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(0);
            Boolean n = r1.lt(r2);
            t(n, false);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(0);
            Boolean n = r1.lt(r2);
            t(n, false);
        }
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(1);
            Boolean n = r1.lt(r2);
            t(n, true);
        }
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(2);
            Boolean n = r1.lt(r2);
            t(n, true);
        }
        {
            Rational r1 = new Rational(-1);
            Rational r2 = new Rational(0);
            Boolean n = r1.lt(r2);
            t(n, true);
        }
        {
            Rational r1 = new Rational(-2);
            Rational r2 = new Rational(-1);
            Boolean n = r1.lt(r2);
            t(n, true);
        }
        end();
    }
    public static void test_gt() {
        beg();
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(0);
            Boolean n = r1.gt(r2);
            t(n, false);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(0);
            Boolean n = r1.gt(r2);
            t(n, true);
        }
        {
            Rational r1 = new Rational(3);
            Rational r2 = new Rational(1);
            Boolean n = r1.gt(r2);
            t(n, true);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(3);
            Boolean n = r1.gt(r2);
            t(n, false);
        }
        {
            Rational r1 = new Rational(3);
            Rational r2 = new Rational(3);
            Boolean n = r1.gt(r2);
            t(n, false);
        }
        {
            Rational r1 = new Rational(-3);
            Rational r2 = new Rational(-4);
            Boolean n = r1.gt(r2);
            t(n, false);
        }
        end();
    }
    public static void test_compareTo() {
        beg();
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(0);
            int n = r1.compareTo(r2);
            t(n, 0);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(1);
            int n = r1.compareTo(r2);
            t(n, 0);
        }
        {
            Rational r1 = new Rational(2);
            Rational r2 = new Rational(1);
            int n = r1.compareTo(r2);
            t(n, 1);
        }
        {
            Rational r1 = new Rational(3);
            Rational r2 = new Rational(1);
            int n = r1.compareTo(r2);
            t(n, 1);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(2);
            int n = r1.compareTo(r2);
            t(n, -1);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(3);
            int n = r1.compareTo(r2);
            t(n, -1);
        }
        {
            // 1/2 - 2/3 = 3/6 - 4/6 = -1/6
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(2, 3);
            int n = r1.compareTo(r2);
            t(n, -1);
        }
        {
            // 1/2 - 2/3 = 3/6 - 4/6 = -1/6
            Rational r1 = new Rational(2, 3);
            Rational r2 = new Rational(1, 2);
            int n = r1.compareTo(r2);
            t(n, 1);
        }
        end();
    }
    public static void test_divide() {
        beg();
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(1);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(1);
            t(r3.equals(r4), true);
        }
        {
            Rational r1 = new Rational(-1);
            Rational r2 = new Rational(1);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(-1);
            t(r3.equals(r4), true);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(-1);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(-1);
            t(r3.equals(r4), true);
        }
        {
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(-1);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(-1, 2);
            t(r3.equals(r4), true);
        }
        {
            Rational r1 = new Rational(-1, 2);
            Rational r2 = new Rational(-1);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(1, 2);
            t(r3.equals(r4), true);
        }
        {
            Rational r1 = new Rational(-1, 2);
            Rational r2 = new Rational(-1, 2);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(1);
            t(r3.equals(r4), true);
        }
        {
            Rational r1 = new Rational(-1, 2);
            Rational r2 = new Rational(1, 2);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(-1);
            t(r3.equals(r4), true);
        }
        {
            // -2/4 / 4/8 = -2/4 * 8/4 = -16/16 = -1
            Rational r1 = new Rational(-2, 4);
            Rational r2 = new Rational(4, 8);
            Rational r3 = r1.divide(r2);
            Rational r4 = new Rational(-1);
            t(r3.equals(r4), true);
        }
    }
    public static void test_inverse() {
        beg();
        {
            Rational r = new Rational(1);
            Rational r1 = new Rational(1);
            t(r.inverse().equals(r1), true);
        }
        {
            Rational r = new Rational(1, 2);
            Rational r1 = new Rational(2, 1);
            t(r.inverse().equals(r1), true);
        }
        {
            Rational r = new Rational(-1, 2);
            Rational r1 = new Rational(-2, 1);
            t(r.inverse().equals(r1), true);
        }
        {
            Rational r = new Rational(-1, -2);
            Rational r1 = new Rational(2, 1);
            t(r.inverse().equals(r1), true);
        }
        {
            Rational r = new Rational(1, -2);
            Rational r1 = new Rational(-2, 1);
            t(r.inverse().equals(r1), true);
        }
        end();
    }
    public static void test_subtract() {
        beg();
        {
            // 0 - 0 = 0
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(0);
            Rational r3 = new Rational(0);
            t(r1.subtract(r2).equals(r3), true);
        }
        {
            // 0 - 0 = 0
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(1);
            Rational r3 = new Rational(0);
            t(r1.subtract(r2).equals(r3), true);
        }
        {
            // 0 - 0 = 0
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(-1);
            Rational r3 = new Rational(2);
            t(r1.subtract(r2).equals(r3), true);
        }
        {
            // 1/2 - 3/4 = 2/4 - 3/4 = -1/4
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(3, 4);
            Rational r3 = new Rational(-1, 4);
            t(r1.subtract(r2).equals(r3), true);
        }
        {
            // -1/2 - 3/4 = -2/4 - 3/4 = -5/4
            Rational r1 = new Rational(-1, 2);
            Rational r2 = new Rational(3, 4);
            Rational r3 = new Rational(-5, 4);
            t(r1.subtract(r2).equals(r3), true);
        }
        {
            // -3/6 - 3/9 = -9/18 - 6/18 = -15/18
            Rational r1 = new Rational(-3, 6);
            Rational r2 = new Rational(3, 9);
            Rational r3 = new Rational(-5, 6);
            t(r1.subtract(r2).equals(r3), true);
        }
    }
    public static void test_negate() {
        beg();
        {
            Rational r = new Rational(0);
            Rational r1 = r.negate();
            t(r.equals(r1), true);
        }
        {
            Rational r = new Rational(1);
            Rational r1 = r.negate();
            Rational r2 = new Rational(-1);
            t(r1.equals(r2), true);
        }
        {
            Rational r = new Rational(-1);
            Rational r1 = r.negate();
            Rational r2 = new Rational(1);
            t(r1.equals(r2), true);
        }
    }
    public static void test_Rational_multi() {
        beg();
        {
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(1);
            Rational rr = r1.multi(r2);
            Rational r3 = new Rational(1, 2);
            t(rr.equals(r3), true);
        }
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(1);
            Rational rr = r1.multi(r2);
            Rational r3 = new Rational(0);
            t(rr.equals(r3), true);
        }
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(0);
            Rational rr = r1.multi(r2);
            Rational r3 = new Rational(0);
            t(rr.equals(r3), true);
        }
        {
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(1, 3);
            Rational rr = r1.multi(r2);
            Rational r3 = new Rational(1, 6);
            t(rr.equals(r3), true);
        }
        {
            Rational r1 = new Rational(2, 4);
            Rational r2 = new Rational(3, 3);
            Rational rr = r1.multi(r2);
            Rational r3 = new Rational(1, 2);
            t(rr.equals(r3), true);
        }
    }
    public static void test_simplify() {
        beg();
        {
            Rational r = new Rational(0);
            Rational r1 = r.simplify();
            Rational exp = new Rational(0);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(1);
            Rational r1 = r.simplify();
            Rational exp = new Rational(1);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(2);
            Rational r1 = r.simplify();
            Rational exp = new Rational(2);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(2, 1);
            Rational r1 = r.simplify();
            Rational exp = new Rational(2, 1);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(2, 4);
            Rational r1 = r.simplify();
            Rational exp = new Rational(1, 2);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(4, 2);
            Rational r1 = r.simplify();
            Rational exp = new Rational(2, 1);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(3, 12);
            Rational r1 = r.simplify();
            Rational exp = new Rational(1, 4);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(-3, 12);
            Rational r1 = r.simplify();
            Rational exp = new Rational(-1, 4);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(-3, -12);
            Rational r1 = r.simplify();
            Rational exp = new Rational(1, 4);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(3, -12);
            Rational r1 = r.simplify();
            Rational exp = new Rational(-1, 4);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(-12, -12);
            Rational r1 = r.simplify();
            Rational exp = new Rational(1, 1);
            t(r1.equals(exp), true);
        }
        {
            Rational r = new Rational(12, 12);
            Rational r1 = r.simplify();
            Rational exp = new Rational(1, 1);
            t(r1.equals(exp), true);
        }
    }
    public static void test_constructor_Rational() {
        beg();
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(1, 1);
            t(r1.equals(r2), true);
        }
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(0, 1);
            t(r1.equals(r2), true);
        }
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(1);
            t(r1.equals(r2), true);
        }
        end();
    }
    public static void test_Rational_add() {
        beg();
        {
            Rational r1 = new Rational(1);
            Rational r2 = new Rational(1);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(2);
            t(exp.equals(r3), true);
        }
        {
            Rational r1 = new Rational(0);
            Rational r2 = new Rational(1);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(1);
            t(exp.equals(r3), true);
        }
        {
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(1);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(3, 2);
            t(exp.equals(r3), true);
        }
        {
            // 1/2 + 2/3 = 3/6 + 4/6
            Rational r1 = new Rational(1, 2);
            Rational r2 = new Rational(2, 3);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(7, 6);
            t(exp.equals(r3), true);
        }
        {
            // 1/4 = 3/12 + 4/12 = 7/12
            Rational r1 = new Rational(1, 4);
            Rational r2 = new Rational(2, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(7, 12);
            t(exp.equals(r3), true);
        }
        {
            // 2/6 + 2/6 = 4/6
            Rational r1 = new Rational(2, 6);
            Rational r2 = new Rational(2, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(2, 3);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(6, 6);
            Rational r2 = new Rational(6, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(2, 1);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(-2, 6);
            Rational r2 = new Rational(6, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(2, 3);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(2, 6);
            Rational r2 = new Rational(-6, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(-2, 3);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(2, -6);
            Rational r2 = new Rational(6, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(2, 3);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(-2, -6);
            Rational r2 = new Rational(6, 6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(4, 3);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(-2, -6);
            Rational r2 = new Rational(-6, -6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(4, 3);
            t(exp.equals(r3), true);
        }
        {
            // 6/6 + 6/6 = 2/1
            Rational r1 = new Rational(0, -6);
            Rational r2 = new Rational(-6, -6);
            Rational r3 = r1.add(r2);
            Rational exp = new Rational(1, 1);
            t(exp.equals(r3), true);
        }
        end();
    }


    public static void test_evaluate_poly() {
        beg();
        {
            List<PNode> s1 = new ArrayList<>();
            List<PNode> s2 = new ArrayList<>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));
            Polynomial p = new Polynomial(s1);
            int n = p.evaluate(0);
            t(n, 0);
        }
//        {
//            List<PNode> s1 = new ArrayList<>();
//            List<PNode> s2 = new ArrayList<>();
//            // 1x^2 + 3x^4 + 6x^5
//            s1.add(new PNode(1, 2));
//            s1.add(new PNode(3, 4));
//            s1.add(new PNode(6, 5));
//            Polynomial p = new Polynomial(s1);
//            int n = p.evaluate(1);
//            t(n, 10);
//        }
//        {
//            List<PNode> s1 = new ArrayList<>();
//            List<PNode> s2 = new ArrayList<>();
//            // 1x^2 + 3x^4 + 6x^5
//            s1.add(new PNode(1, 2)); // 1 x 4 = 4
//            s1.add(new PNode(3, 4)); // 3 x 16 = 48
//            Polynomial p = new Polynomial(s1);
//            int n = p.evaluate(2);
//            t(n, 52);
//        }
        end();
    }

    public static void test_evaluate() {
        beg();
        {
            PNode node = new PNode(2, 3);
            int n = node.evaluate(0);
            t(n, 0);
        }
        {
            PNode node = new PNode(2, 3);
            int n = node.evaluate(1);
            t(n, 2);
        }
        {
            PNode node = new PNode(2, 3);
            int n = node.evaluate(2);
            t(n, 16);
        }
        {
            PNode node = new PNode(0, 3);
            int n = node.evaluate(2);
            t(n, 0);
        }
        {
            PNode node = new PNode(1, 1);
            int n = node.evaluate(1);
            t(n, 1);
        }
        end();
    }

    public static void test_multiply() {
        beg();
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            s1.add(new PNode(1, 2));
            Polynomial p1 = new Polynomial(s1);

            s2.add(new PNode(4, 3));
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(new PNode(4, 5));
            Polynomial expectedPoly = new Polynomial(expected);
            fl("t(expected, p3.poly)");
            t(expectedPoly.poly, p3.poly);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            s1.add(PNode.zero());

            s2.add(new PNode(4, 3));

            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(PNode.zero());
            fl("t(expected, p3.poly)");
            t(expected, p3.poly);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            s1.add(new PNode(1, 2));

            s2.add(new PNode(4, 2));

            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(new PNode(4, 4));
            Polynomial exppoly = new Polynomial(expected);
            fl("t(expected, p3.poly)");
            t(p3.equals(exppoly), true);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            // 1x^2 + 3x^4 + 6x^5
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));
            // 4x^2 + 2x^5 + 4x^9
            s2.add(new PNode(4, 2));
            s2.add(new PNode(2, 5));
            s2.add(new PNode(4, 9));
            // 4x^4 + 12x^6 + 24x^7
            // 2x^7 + 6x^9 + 12x^10
            // 4x^11 + 12x^13 + 24x^14
            // => 4x^4 + 12x^6 + 26x^7 + 6x^9 + 12x^10 + 4x^11 + 12x^13 + 24x^14


            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(new PNode(4, 4));
            expected.add(new PNode(12, 6));
            expected.add(new PNode(26, 7));
            expected.add(new PNode(6, 9));
            expected.add(new PNode(12, 10));
            expected.add(new PNode(4, 11));
            expected.add(new PNode(12, 13));
            expected.add(new PNode(24, 14));
            Polynomial exppoly = new Polynomial(expected);
            fl("t(expected, p3.poly)");
            t(p3.equals(exppoly), true);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            // 1x^2 + 3x^4 + 6x^5
            s1.add(PNode.zero());
            // 4x^2 + 2x^5 + 4x^9
            s2.add(new PNode(4, 2));
            s2.add(new PNode(2, 5));
            s2.add(new PNode(4, 9));
            // 4x^4 + 12x^6 + 24x^7
            // 2x^7 + 6x^9 + 12x^10
            // 4x^11 + 12x^13 + 24x^14
            // => 4x^4 + 12x^6 + 26x^7 + 6x^9 + 12x^10 + 4x^11 + 12x^13 + 24x^14


            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(PNode.zero());
            Polynomial exppoly = new Polynomial(expected);
            fl("t(expected, p3.poly)");
            t(p3.equals(exppoly), true);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            // 1x^2 + 3x^4 + 6x^5
            s1.add(new PNode(1, 0));
            // 4x^2 + 2x^5 + 4x^9
            s2.add(new PNode(4, 2));
            s2.add(new PNode(2, 5));
            // 4x^4 + 12x^6 + 24x^7
            // 2x^7 + 6x^9 + 12x^10
            // 4x^11 + 12x^13 + 24x^14
            // => 4x^4 + 12x^6 + 26x^7 + 6x^9 + 12x^10 + 4x^11 + 12x^13 + 24x^14


            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(new PNode(4, 2));
            expected.add(new PNode(2, 5));
            Polynomial exppoly = new Polynomial(expected);
            fl("t(expected, p3.poly)");
            t(p3.equals(exppoly), true);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            // 1x^2 + 3x^4 + 6x^5
            s1.add(new PNode(1, 0));
            // 4x^2 + 2x^5 + 4x^9
            s2.add(new PNode(4, 0));
            // 4x^4 + 12x^6 + 24x^7
            // 2x^7 + 6x^9 + 12x^10
            // 4x^11 + 12x^13 + 24x^14
            // => 4x^4 + 12x^6 + 26x^7 + 6x^9 + 12x^10 + 4x^11 + 12x^13 + 24x^14


            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.multiply(p2);

            List<PNode> expected = new ArrayList<PNode>();
            expected.add(new PNode(4, 0));
            Polynomial exppoly = new Polynomial(expected);
            fl("t(expected, p3.poly)");
            t(p3.equals(exppoly), true);
        }
        end();
    }

    public static void test_addPoly() {
        beg();
        {
            List<PNode> s1 = new ArrayList<>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));

            List<PNode> s2 = new ArrayList<>();
            s2.add(new PNode(4, 2));
            s2.add(new PNode(2, 5));
            s2.add(new PNode(4, 9));

            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            Polynomial p3 = p1.addPoly(p2);
            fl("p1");
            p1.print();
            fl("p2");
            p2.print();
            fl("p3");
            p3.print();

            List<PNode> expected = new ArrayList<>();
            expected.add(new PNode(5, 2));
            expected.add(new PNode(3, 4));
            expected.add(new PNode(8, 5));
            expected.add(new PNode(4, 9));
            Polynomial expoly = new Polynomial(expected);
            fl("expoly.poly p3.poly");
            t(p3.equals(expoly), true);
        }
        end();
    }

    // 4  <3> 2
    public static void test1() {
        beg();
        {
            ArrayList<Node> ls = new ArrayList<>();
            ls.add(new Node(2));
            ls.add(new Node(4));
            ls.add(new Node(9));
            ls.add(new Node(9));
            ls.add(new Node(9));

            BiFunction<Node, Node, Boolean> f = (x, y) -> x.data - y.data == 0;
            fl("lss");
            List<List<Node>> lss = groupBy(f, ls);
            fl("printList2d");
            printList2d(lss);

            ArrayList<Integer> mylist = new ArrayList<>();
            for (List<Node> els : lss) {
                Integer s = sum(map2((x) -> x.data, els));
                mylist.add(s);
            }
            fl("mylist");
            printList(mylist);
        }
        {
            List<String> list9 = Arrays.asList("cat1", "dog1", "cow1");
            String rstr = drop(1, list9.stream().reduce("-", (x, y) -> x + "," + y));
            Print.p("rstr=" + rstr);
        }


        end();
    }

    public static void test_add() {
        beg();
        {
            PNode pnode1 = new PNode(0, 1);
            PNode pnode2 = PNode.zero();
            PNode n3 = pnode1.add(pnode2);
            PNode expected = new PNode(0, 1);
            t(n3, expected);
        }
        {
            PNode pnode1 = PNode.zero();
            PNode pnode2 = new PNode(0, 1);
            PNode n3 = pnode1.add(pnode2);
            PNode expected = new PNode(0, 1);
            t(n3, expected);
        }
        {
            PNode pnode1 = new PNode(0, 1);
            PNode pnode2 = new PNode(2, 1);
            PNode n3 = pnode1.add(pnode2);
            PNode expected = new PNode(2, 1);
            t(n3, expected);
        }
        {
            List<PNode> list = repeat(1, PNode.zero());
            List<PNode> expected = new ArrayList<PNode>();
            expected.add(PNode.zero());
            t(list, expected);
        }
        {
            List<PNode> list = repeat(2, new PNode(1, 2));
            List<PNode> expected = new ArrayList<PNode>();
            expected.add(new PNode(1, 2));
            expected.add(new PNode(1, 2));
            t(list, expected);
        }
        {
            PNode pnode1 = new PNode(0, 1);
            PNode pnode2 = new PNode(2, 1);
            PNode n3 = pnode1.add(pnode2);
            PNode expected = new PNode(2, 1);
            t(n3, expected);
        }
        {
            PNode pnode1 = new PNode(0, 2);
            PNode pnode2 = new PNode(2, 1);
            PNode n3 = pnode1.add(pnode2);
            PNode expected = new PNode(2, 1);
            t(n3, expected);
        }
        {
            PNode pnode1 = new PNode(0, 2);
            PNode pnode2 = new PNode(0, 1);
            PNode n3 = pnode1.add(pnode2);
            PNode expected = PNode.zero();
            t(n3, expected);
        }
        {
            PNode pnode1 = new PNode(3, 2);
            PNode pnode2 = new PNode(-3, 2);
            PNode n3 = pnode1.add(pnode2);
            PNode expected = new PNode(0, 2);
            t(n3, expected);
        }
        end();
    }

    public static void testfillzero() {
        beg();
        {
            PNode pnode1 = PNode.zero();
            List<PNode> ls = new ArrayList<PNode>();
            ls.add(pnode1);

            Polynomial p = new Polynomial(ls);
            Polynomial p1 = p.fillZero();
            PNode exnode = PNode.zero();
            List<PNode> expected = new ArrayList<>();
            expected.add(exnode);
            t(p1.poly, expected);
        }
        {
            PNode pnode1 = new PNode(0, 1);
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);

            Polynomial p = new Polynomial(ls);
            Polynomial p1 = p.fillZero();
            PNode exnode = PNode.zero();
            List<PNode> expected = new ArrayList<>();
            expected.add(exnode);
            t(p1.poly, expected);
        }
        {
            PNode pnode1 = new PNode(1, 1);
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);

            Polynomial p = new Polynomial(ls);
            Polynomial p1 = p.fillZero();
            PNode n0 = PNode.zero();
            PNode n1 = new PNode(1, 1);
            List<PNode> expected = new ArrayList<>();
            expected.add(n0);
            expected.add(n1);
            t(p1.poly, expected);
        }
        {
            PNode pnode1 = new PNode(1, 1);
            PNode pnode2 = new PNode(2, 3);
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);
            ls.add(pnode2);

            Polynomial p = new Polynomial(ls);
            Polynomial p1 = p.fillZero();
            PNode n0 = PNode.zero();
            PNode n1 = new PNode(1, 1);
            PNode n3 = new PNode(2, 3);
            List<PNode> expected = new ArrayList<>();
            expected.add(n0);
            expected.add(n1);
            expected.add(n3);
            t(p1.poly, expected);
        }
        end();
    }

    public static void testnegate() {
        beg();
        {
            PNode pnode1 = PNode.zero();
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);

            Polynomial p = new Polynomial(ls);
            Polynomial pn = p.negate();
            List<PNode> expected = new ArrayList<>();
            expected.add(PNode.zero());
            Polynomial exppoly = new Polynomial(expected);
            t(pn.equals(exppoly), true);
        }
        {
            PNode pnode1 = new PNode(0, 1);
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);

            Polynomial p = new Polynomial(ls);
            Polynomial pn = p.negate();
            List<PNode> expected = new ArrayList<>();
            expected.add(new PNode(0, 1));
            Polynomial exppoly = new Polynomial(expected);
            t(pn.equals(exppoly));
        }
        {
            PNode pnode1 = new PNode(1, 1);
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);

            Polynomial p = new Polynomial(ls);
            Polynomial pn = p.negate();
            List<PNode> expected = new ArrayList<>();
            expected.add(new PNode(-1, 1));
            Polynomial exppoly = new Polynomial(expected);
            t(pn.equals(exppoly));
        }
        {
            PNode pnode1 = new PNode(1, 1);
            PNode pnode2 = new PNode(-2, 2);
            List<PNode> ls = new ArrayList<>();
            ls.add(pnode1);
            ls.add(pnode2);

            Polynomial p = new Polynomial(ls);
            Polynomial pn = p.negate();
            List<PNode> expected = new ArrayList<>();
            expected.add(new PNode(-1, 1));
            expected.add(new PNode(2, 2));
            Polynomial exppoly = new Polynomial(expected);
            t(pn.equals(exppoly), true);

        }
        end();
    }

    public static void test_degree() {
        beg();
        {
            List<PNode> s1 = of();
            List<PNode> s2 = of();
            s1.add(new PNode(0, 2));

            Polynomial p1 = new Polynomial(s1);
            int n1 = p1.degree();
            t(n1, NEGATIVE_ONE);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            List<PNode> s2 = new ArrayList<PNode>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));

            s2.add(new PNode(4, 2));
            s2.add(new PNode(2, 5));
            s2.add(new PNode(4, 9));
            Polynomial p1 = new Polynomial(s1);
            Polynomial p2 = new Polynomial(s2);
            int n1 = p1.degree();
            int n2 = p2.degree();
            t(n1, 5);
            t(n2, 9);
        }
        end();
    }



    public static void test_diff() {
        beg();
        {
            // f(x) = 0 => diff(f) = 0
            List<PNode> s1 = of();
            s1.add(PNode.zero());
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = of();
            s2.add(new PNode(0, NEGATIVE_ONE));
            Polynomial p2 = new Polynomial(s2);

            Polynomial p = p1.diff();
            t(p.equals(p2), true);
        }
        {
            // f(x) = 3 => diff(f) = 0
            List<PNode> s1 = of();
            s1.add(new PNode(3, 0));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = of();
            s2.add(PNode.zero());
            Polynomial p2 = new Polynomial(s2);

            Polynomial p = p1.diff();
            t(p.equals(p2), true);
        }
        {
            // f(x) = 0 + x^2 => diff(f) = 0 + 2x^1
            List<PNode> s1 = of();
            s1.add(PNode.zero());
            s1.add(new PNode(1, 2));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = of();
            s2.add(new PNode(2, 1));
            Polynomial p2 = new Polynomial(s2);

            Polynomial p = p1.diff();
            t(p.equals(p2), true);
        }
        {
            // f(x) = 0 + 3 + x^2 => diff(f) = 0 + 2x^1
            List<PNode> s1 = of();
            s1.add(PNode.zero());
            s1.add(new PNode(3, 0));
            s1.add(new PNode(1, 2));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = of();
            s2.add(new PNode(2, 1));
            Polynomial p2 = new Polynomial(s2);

            Polynomial p = p1.diff();
            t(p.equals(p2), true);
        }
        {
            // f(x) = 0 + 2x^2 => diff(f) = 0 + 4x^1
            List<PNode> s1 = of();
            s1.add(PNode.zero());
            s1.add(new PNode(1, 2));
            s1.add(new PNode(1, 2));

            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = of();
            s2.add(new PNode(4, 1));
            Polynomial p2 = new Polynomial(s2);

            Polynomial p = p1.diff();
            t(p.equals(p2), true);
        }
        {
            // f(x) = 0 + 2x^2 + 4x^5 => diff(f) = 0 + 4x^1 + 20x^4
            List<PNode> s1 = of();
            s1.add(PNode.zero());
            s1.add(new PNode(1, 2));
            s1.add(new PNode(1, 2));
            s1.add(new PNode(4, 5));

            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = of();
            s2.add(new PNode(4, 1));
            s2.add(new PNode(20, 4));
            Polynomial p2 = new Polynomial(s2);

            Polynomial p = p1.diff();
            t(p.equals(p2), true);
        }
        end();
    }

    public static void test_poly_equals() {
        beg();
        {
            List<PNode> s1 = new ArrayList<PNode>();
            s1.add(new PNode(0, NEGATIVE_ONE));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = new ArrayList<PNode>();
            s1.add(new PNode(0, NEGATIVE_ONE));
            Polynomial p2 = new Polynomial(s1);

            t(p1.equals(p2), true);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            s1.add(PNode.zero());
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = new ArrayList<PNode>();
            s1.add(new PNode(0, NEGATIVE_ONE));
            Polynomial p2 = new Polynomial(s1);

            t(p1.equals(p2), true);
        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = new ArrayList<PNode>();
            s2.add(new PNode(1, 2));
            s2.add(new PNode(3, 4));
            s2.add(new PNode(6, 5));
            Polynomial p2 = new Polynomial(s2);

            t(p1.equals(p2), true);

        }
        {
            List<PNode> s1 = new ArrayList<PNode>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = new ArrayList<PNode>();
            s2.add(new PNode(1, 2));
            s2.add(new PNode(3, 4));
            s2.add(new PNode(6, 5));
            s2.add(PNode.zero());
            Polynomial p2 = new Polynomial(s2);

            Polynomial p3 = p2.normal();

            t(p3.equals(p2), true);

        }
        {
            List<PNode> s1 = new ArrayList<>();
            s1.add(new PNode(1, 2));
            Polynomial p1 = new Polynomial(s1);
            PNode n1 = new PNode(0, NEGATIVE_ONE);
            PNode n2 = new PNode(1, 2);
            List<PNode> list2 = of();
            list2.add(n1);
            list2.add(n2);

            Polynomial p2 = new Polynomial(list2);

            t(p1.poly.equals(p2.poly), true);
        }
        end();
    }

    public static void test_normal(){
        beg();
        {
            List<PNode> s2 = new ArrayList<PNode>();
            s2.add(new PNode(1, 2));
            s2.add(new PNode(3, 4));
            s2.add(new PNode(6, 5));
            s2.add(PNode.zero());
            Polynomial p2 = new Polynomial(s2);

            List<PNode> s3 = new ArrayList<PNode>();
            s3.add(new PNode(1, 2));
            s3.add(new PNode(3, 4));
            s3.add(new PNode(6, 5));

            Polynomial p3 = new Polynomial(s3);

            Polynomial p4 = p2.normal();
            fl("p3");
            p3.print();
            fl("p4");
            p4.print();

            t(p3.equals(p4), true);

        }
        end();
    }
    public static void test_normalList() {
        beg();
        {
            List<PNode> s1 = new ArrayList<>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = new ArrayList<>();
            s2.add(new PNode(1, 2));
            s2.add(new PNode(3, 4));
            s2.add(new PNode(6, 5));
            //s2.add(PNode.zero());

            Polynomial p2 = new Polynomial(s2);

            fl("p1.print()");
            p1.print();
            fl("p2.print()");
            p2.print();
            fl("p1.equals.(p2)");
            t(p1.equals(p2), true);
        }
        {
            List<PNode> s1 = new ArrayList<>();
            s1.add(new PNode(1, 2));
            s1.add(new PNode(3, 4));
            s1.add(new PNode(6, 5));
            s1.add(PNode.zero());
            Polynomial p1 = new Polynomial(s1);

            List<PNode> s2 = new ArrayList<>();
            s2.add(new PNode(1, 2));
            s2.add(new PNode(3, 4));
            s2.add(new PNode(6, 5));
            //s2.add(PNode.zero());

            Polynomial p2 = new Polynomial(s2);

            fl("p1.print()");
            p1.print();
            fl("p2.print()");
            p2.print();
            fl("p1.equals.(p2)");
            t(p1.equals(p2), true);
        }

        end();
    }

    public static void test_Rational_equals(){
        beg();
        {
            Rational r1 = new Rational(1, 1);
            Rational r2 = new Rational(1, 1);
            t(r1.equals(r2), true);

        }
        end();
    }

    public static void test_pow_int() {
        beg();
        {
            int b = 1;
            int n = 0;
            t(pow(b, n), 1);
        }
        {
            int b = 1;
            int n = 1;
            t(pow(b, n), 1);
        }
        {
            int b = 2;
            int n = 1;
            t(pow(b, n), 2);
        }
        {
            int b = 1;
            int n = 2;
            t(pow(b, n), 1);
        }
        {
            int b = 2;
            int n = 2;
            t(pow(b, n), 4);
        }
        {
            int b = 2;
            int n = 3;
            t(pow(b, n), 8);
        }
        end();
    }
        public static void test_pow_long(){
        beg();
        {
            long b = 1;
            long n = 0;
            t(pow(b, n), 1);
        }
        {
            long b = 1;
            long n = 1;
            t(pow(b, n), 1);
        }
        {
            long b = 2;
            long n = 1;
            t(pow(b, n), 2);
        }
        {
            long b = 1;
            long n = 2;
            t(pow(b, n), 1);
        }
        {
            long b = 2;
            long n = 2;
            t(pow(b, n), 4);
        }
        {
            long b = 2;
            long n = 3;
            t(pow(b, n), 8);
        }
        end();
    }

    public static void test_pow_Rational(){
        beg();
        {
            Rational r = new Rational(0);
            Rational r1 = r.pow(0);
            t(r1.equals(new Rational(1)));
        }
        {
            Rational r = new Rational(1);
            Rational r1 = r.pow(0);
            t(r1.equals(new Rational(1)));
        }
        {
            Rational r = new Rational(1);
            Rational r1 = r.pow(1);
            t(r1.equals(new Rational(1)));
        }
        {
            Rational r = new Rational(2);
            Rational r1 = r.pow(1);
            t(r1.equals(new Rational(2)));
        }
        {
            Rational r = new Rational(2, 3);
            Rational r1 = r.pow(0);
            t(r1.equals(new Rational(1)));
        }
        {
            Rational r = new Rational(2, 3);
            Rational r1 = r.pow(1);
            t(r1.equals(new Rational(2, 3)));
        }
        {
            Rational r = new Rational(2, 3);
            Rational r1 = r.pow(2);
            t(r1.equals(new Rational(4, 9)));
        }
        {
            Rational r = new Rational(2, 3);
            Rational r1 = r.pow(3);
            t(r1.equals(new Rational(8, 27)));
        }
        end();
    }

    public static void test_99(){
        beg();
        List<Integer> ls = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> ret = multiLongList(ls, ls);
        p(ret);
        end();
    }

    public static Polynomial createPoly() {
        List<PNode> s1 = new ArrayList<>();
        s1.add(new PNode(1, 2));
        s1.add(new PNode(3, 4));
        s1.add(new PNode(6, 5));

        Polynomial p1 = new Polynomial(s1);

        return p1;
    }

}


